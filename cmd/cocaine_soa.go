package main

import (
	"github.com/cocaine/cocaine-framework-go/cocaine"
	"bitbucket.org/asolomonov/soa/controllers"
	"log"
)

func main() {
	binds := map[string]cocaine.EventHandler{
		"echo": controllers.HelloCocaine,
	}
	Worker, err := cocaine.NewWorker()
	if err != nil {
		log.Fatal(err)
	}
	Worker.Loop(binds)
}
