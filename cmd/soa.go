package main

import (
	"bitbucket.org/asolomonov/soa/controllers"
	"bitbucket.org/asolomonov/soa/models"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"runtime"
)

func main() {
	defer models.Close()

	runtime.GOMAXPROCS(runtime.NumCPU())
	//	runtime.GOMAXPROCS(1)

	r := mux.NewRouter()
	r.NotFoundHandler = http.HandlerFunc(controllers.Forbidden)
	r.HandleFunc("/hello", controllers.Hello)
	r.HandleFunc("/getBalance/{userId:[0-9]+}/{currencyId:[0-9]+}", controllers.GetBalance)
	r.HandleFunc("/moveAmount", controllers.MoveAmount).Methods("POST")
	http.Handle("/", r)

	err := http.ListenAndServe(":4321", nil)
	if err != nil {
		fmt.Println(err)
	}
}
