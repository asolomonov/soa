package controllers

import (
	"net/http"
	"encoding/json"
)

const (
	UNPROCESSABLE_ENTITY_CODE int = 422
)

type Response struct {
	Result  interface{} `json:"result,omitempty"`
	Results interface{} `json:"results,omitempty"`
	Code    int         `json:"code"`
	Error   string      `json:"error,omitempty"`
}

func createResponseByCode(code int) *Response {
	return &Response{Code: code}
}

func showResult(rw http.ResponseWriter, result interface{}) {
	resp := createResponseByCode(http.StatusOK)
	resp.Result = result
	writeResponse(rw, resp)
}

func showResults(rw http.ResponseWriter, results interface{}) {
	resp := createResponseByCode(http.StatusOK)
	resp.Results = results
	writeResponse(rw, resp)
}

func showError(rw http.ResponseWriter, code int, message string) {
	resp := createResponseByCode(code)
	resp.Error = message
	writeResponse(rw, resp)
}

func writeResponse(rw http.ResponseWriter, resp *Response) {
	rw.WriteHeader(resp.Code)
	json.NewEncoder(rw).Encode(resp)
}
