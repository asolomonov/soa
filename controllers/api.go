package controllers

import (
	"github.com/cocaine/cocaine-framework-go/cocaine"
	"bitbucket.org/asolomonov/soa/models"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

var (
	helloWorld = []byte{'H', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd'}
)

func Forbidden(rw http.ResponseWriter, req *http.Request) {
	showError(rw, http.StatusForbidden, "forbidden")
}

func GetBalance(rw http.ResponseWriter, req *http.Request) {
	wallet := new(models.Wallet)
	vars := mux.Vars(req)
	userId, userIdErr := strconv.Atoi(vars["userId"])
	currencyId, currencyErrId := strconv.Atoi(vars["currencyId"])
	if userIdErr != nil || currencyErrId != nil {
		showError(rw, http.StatusBadRequest, "bad request")
		return
	}
	if !isValidCurrencyId(currencyId) {
		showError(rw, http.StatusBadRequest, "invalid currency")
		return
	}
	models.GetWalletDAO().FindByUserIdAndCurrencyId(wallet, userId, currencyId)
	showResult(rw, wallet)
}

func isValidCurrencyId(currencyId int) bool {
	return currencyId >= models.RUR_CURRENCY || currencyId <= models.KZT_CURRENCY
}

func MoveAmount(rw http.ResponseWriter, req *http.Request) {
	fromUserId, fromUserIdErr := strconv.Atoi(req.PostFormValue("fromUserId"))
	toUserId, toUserIdErr := strconv.Atoi(req.PostFormValue("toUserId"))
	currencyId, currencyIdErr := strconv.Atoi(req.PostFormValue("currencyId"))
	amount, amountErr := strconv.ParseFloat(req.PostFormValue("amount"), 32)
	if fromUserIdErr != nil || toUserIdErr != nil || currencyIdErr != nil || amountErr != nil {
		showError(rw, http.StatusBadRequest, "bad request")
		return
	}
	if !isValidCurrencyId(currencyId) {
		showError(rw, http.StatusBadRequest, "invalid currency")
		return
	}
	tx, err := models.DB().Begin()
	if err != nil {
		showError(rw, http.StatusInternalServerError, "ooops#1...")
		return
	}

	fromWallet := new(models.Wallet)
	findOrCreateWallet(fromWallet, fromUserId, currencyId)
	toWallet := new(models.Wallet)
	findOrCreateWallet(toWallet, toUserId, currencyId)

	if fromWallet.Balance < amount {
		showError(rw, UNPROCESSABLE_ENTITY_CODE, "bad balance")
		return
	}

	walletDao := models.GetWalletDAO()
	fromWallet.Balance -= amount
	walletDao.Save(fromWallet)
	toWallet.Balance += amount
	walletDao.Save(toWallet)

	transactionDAO := models.GetTransactionDAO()
	toTransaction := &models.Transaction{FromWalletId: fromWallet.Id, ToWalletId: toWallet.Id, Amount: amount}
	transactionDAO.Create(toTransaction)
	fromTransaction := &models.Transaction{FromWalletId: toWallet.Id, ToWalletId: fromWallet.Id, Amount: -amount}
	transactionDAO.Create(fromTransaction)

	err = tx.Commit()
	if err != nil {
		showError(rw, http.StatusInternalServerError, "ooops#2...")
		return
	}

	showResult(rw, &models.TransactionLog{Transactions: []int{toTransaction.Id, fromTransaction.Id}})
}

func findOrCreateWallet(wallet *models.Wallet, userId, currencyId int) {
	dao := models.GetWalletDAO()
	dao.FindByUserIdAndCurrencyId(wallet, userId, currencyId)
	if wallet.Id == models.INVALID_ID {
		wallet.UserId = userId
		wallet.CurrencyId = currencyId
		dao.Create(wallet)
	}
}

func Hello(rw http.ResponseWriter, req *http.Request) {
	rw.Write(helloWorld)
}

func HelloCocaine(request *cocaine.Request, response *cocaine.Response) {
	inc := <-request.Read()
	response.Write(inc)
	response.Close()
}
