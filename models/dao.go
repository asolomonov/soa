package models

import (
	"database/sql"
	"fmt"
)

type DAO interface {
	PrepareStatements(db *sql.DB)
	QueryRow(interface{}, *sql.Stmt, ...interface{})
	Scan(*sql.Row, interface{})
}

type BaseDAO struct {
	insert, update *sql.Stmt
}

func (this *BaseDAO) queryRow(dao DAO, model interface{}, stmt *sql.Stmt, args ...interface{}) {
	row := stmt.QueryRow(args...)
	if row != nil {
		dao.Scan(row, model)
	}
}

func (this *BaseDAO) exec(dao DAO, stmt *sql.Stmt, args ...interface{}) {
	stmt.Exec(args...)
}

func (this *BaseDAO) сreateStmt(sql string) *sql.Stmt {
	stmt, err := db.Prepare(sql)
	if err == nil {
		return stmt
	} else {
		fmt.Println(err)
		return nil
	}
}
