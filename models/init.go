package models

import (
	_ "github.com/lib/pq"
	"database/sql"
	"fmt"
)

const (
	INVALID_ID           = 0
	MAX_CONNECTION_COUNT = 50
)

var (
	db *sql.DB
	daos []DAO
)

func init() {
	var err error
//	db, err = sql.Open("postgres", "postgres://solomonov:solomonov@192.168.13.33:5432/soa?sslmode=disable&client_encoding=utf-8")
	db, err = sql.Open("postgres", "postgres://byorty:MK99rc@localhost:5432/soa?sslmode=disable&client_encoding=utf-8")
	if err != nil {
		fmt.Println(err)
	}

	db.SetMaxIdleConns(MAX_CONNECTION_COUNT)
	db.SetMaxOpenConns(MAX_CONNECTION_COUNT)

	daos = []DAO {
		GetWalletDAO(),
		GetTransactionDAO(),
	}

	for _, dao := range daos {
		dao.PrepareStatements(db)
	}
}

func DB() *sql.DB {
	return db
}

func Close() {
	db.Close()
}



