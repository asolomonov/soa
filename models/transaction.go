package models

import (
	"database/sql"
	"fmt"
)

var (
	transactionDAO = new(TransactionDAO)
)

type Transaction struct {
	Id           int
	FromWalletId int
	ToWalletId   int
	Amount       float64
}

type TransactionLog struct {
	Transactions []int `json:"transactions"`
}

type TransactionDAO struct {
	BaseDAO
}

func GetTransactionDAO() *TransactionDAO {
	return transactionDAO
}

func (this *TransactionDAO) PrepareStatements(db *sql.DB) {
	this.insert = this.сreateStmt(`INSERT INTO "transaction"("from_wallet_id", "to_wallet_id", "amount") VALUES($1, $2, $3) RETURNING "id", "from_wallet_id", "to_wallet_id", "amount"`)
	this.update = this.сreateStmt(`UPDATE "transaction" SET "from_wallet_id" = $1, "to_wallet_id" = $2 , "amount" = $3 WHERE "id" = $4`)
}

func (this *TransactionDAO) QueryRow(model interface{}, stmt *sql.Stmt, args ...interface{}) {
	this.queryRow(this, model, stmt, args...)
}

func (this *TransactionDAO) Scan(row *sql.Row, model interface{}) {
	transaction := model.(*Transaction)
	err := row.Scan(&transaction.Id, &transaction.FromWalletId, &transaction.ToWalletId, &transaction.Amount)
	if err != nil {
		fmt.Println(err)
	}
}

func (this *TransactionDAO) Create(transaction *Transaction) {
	this.queryRow(this, transaction, this.insert, transaction.FromWalletId, transaction.ToWalletId, transaction.Amount)
}

func (this *TransactionDAO) Save(transaction *Transaction) {
	this.exec(this, this.update, transaction.FromWalletId, transaction.ToWalletId, transaction.Amount, transaction.Id)
}
