package models

import (
	"database/sql"
	"fmt"
)

const (
	RUR_CURRENCY int = iota + 1
	USD_CURRENCY
	EUR_CURRENCY
	UAH_CURRENCY
	BRL_CURRENCY
	KZT_CURRENCY
)

var (
	walletDAO = new(WalletDAO)
)

type Wallet struct {
	Id         int     `json:"-"`
	UserId     int     `json:"-"`
	CurrencyId int     `json:"-"`
	Balance    float64 `json:"balance"`
}

type WalletDAO struct {
	BaseDAO
	byId, byUserIdAndCurrencyId *sql.Stmt
}

func GetWalletDAO() *WalletDAO {
	return walletDAO
}

func (this *WalletDAO) PrepareStatements(db *sql.DB) {
	this.byId = this.сreateStmt(`SELECT "id", "user_id", "currency_id", "balance" FROM "wallet" WHERE "id" = $1`)
	this.byUserIdAndCurrencyId = this.сreateStmt(`SELECT "id", "user_id", "currency_id", "balance" FROM "wallet" WHERE "user_id" = $1 AND "currency_id" = $2`)
	this.insert = this.сreateStmt(`INSERT INTO "wallet"("user_id", "currency_id", "balance") VALUES($1, $2, $3) RETURNING "id", "user_id", "currency_id", "balance"`)
	this.update = this.сreateStmt(`UPDATE "wallet" SET "user_id" = $1, "currency_id" = $2 , "balance" = $3 WHERE "id" = $4`)
}

func (this *WalletDAO) FindById(wallet *Wallet, id int) {
	this.QueryRow(wallet, this.byId, id)
}

func (this *WalletDAO) FindByUserIdAndCurrencyId(wallet *Wallet, userId, currencyId int) {
	this.QueryRow(wallet, this.byUserIdAndCurrencyId, userId, currencyId)
}

func (this *WalletDAO) QueryRow(model interface{}, stmt *sql.Stmt, args ...interface{}) {
	this.queryRow(this, model, stmt, args...)
}

func (this *WalletDAO) Scan(row *sql.Row, model interface{}) {
	wallet := model.(*Wallet)
	err := row.Scan(&wallet.Id, &wallet.UserId, &wallet.CurrencyId, &wallet.Balance)
	if err != nil {
		fmt.Println(err)
	}
}

func (this *WalletDAO) Create(wallet *Wallet) {
	this.queryRow(this, wallet, this.insert, wallet.UserId, wallet.CurrencyId, wallet.Balance)
}

func (this *WalletDAO) Save(wallet *Wallet) {
	this.exec(this, this.update, wallet.UserId, wallet.CurrencyId, wallet.Balance, wallet.Id)
}
